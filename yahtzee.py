import enum
import unittest


class Roll:
    def __init__(self, *dice: [int]):
        if len(dice) == 1:
            dice = dice[0]

        for v in dice:
            if not 0 < v < 7:
                raise ValueError("dice value must be in range [1, 6]")

        if len(dice) != 5:
            raise ValueError("len(dice) != 5")
        self.dice = dice

    def total_face_value(self) -> int:
        return sum(self.dice)


@enum.unique
class LowerSet(enum.Enum):
    def __init__(self, validate, score):
        self.validate = validate
        self.score = score

    @staticmethod
    def roll_is_lower_set(dices: Roll) -> bool:
        pass

    @staticmethod
    def _validate_3oak(dices: Roll) -> bool:
        pass

    @staticmethod
    def _validate_4oak(dices: Roll) -> bool:
        pass

    @staticmethod
    def _validate_full_house(dices: Roll) -> bool:
        pass

    @staticmethod
    def _validate_small_straight(dices: Roll) -> bool:
        pass

    @staticmethod
    def _validate_large_straight(dices: Roll) -> bool:
        pass

    @staticmethod
    def _validate_yahtzee(dices: Roll) -> bool:
        pass

    @staticmethod
    def _validate_chance(dices: Roll) -> bool:
        pass

    @staticmethod
    def _score_total(dices: Roll) -> int:
        return dices.total_face_value()

    def matches_roll(self, dices: Roll):
        pass

    three_of_a_kind = (_validate_3oak, _score_total)
    four_of_a_kind = (_validate_4oak, _score_total)
    full_house = (_validate_full_house, lambda x: 25)
    small_straight = (_validate_small_straight, lambda x: 30)
    large_straight = (_validate_large_straight, lambda x: 40)
    yahtzee = (_validate_yahtzee, lambda x: 50)
    chance = (_validate_chance, _score_total)


class TestRoll(unittest.TestCase):
    def test_roll_constructor(self):
        self.assertRaises(ValueError, Roll, 1, 2, 3, 4, 5, 6)
        self.assertRaises(ValueError, Roll, 1, 2, 3, 4)
        self.assertRaises(ValueError, Roll, [1, 2, 3, 4, 5, 6])
        self.assertRaises(ValueError, Roll, [3, 4, 5, 6])
        self.assertRaises(TypeError, Roll, 1)
        Roll(1, 2, 3, 4, 5)
        Roll([3, 3, 3, 4, 5])
        self.assertRaises(ValueError, Roll, 0, 1, 2, 3, 4)

    def test_total(self):
        self.assertEqual(Roll(1, 2, 3, 4, 5).total_face_value(), 15)
        self.assertEqual(Roll(6, 6, 6, 6, 6).total_face_value(), 30)


if __name__ == "__main__":
    unittest.main()








